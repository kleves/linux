#!/bin/bash 
#Autor: Reginaldo 
#Criado em: 26/06/2011 
#Função: Regras iptables de configuração do firewall 
#Utilização: 
#no Slackware -&gt; /etc/rc.d/rc.firewall2 e colocar em /etc/rc.d/rc.local 
#Start|Stop|Restart 
#file executable: chmod 755 /etc/rc.d/rc.firewall2 
#Variaveis 
ifaceExt="eth0" #acesso internet 
ifaceInt="eth1" #acesso intranet (LAN) 
LAN="192.168.1.0/24" #rede local 
#carrega módulos 
  /sbin/modprobe ip_nat 
  /sbin/modprobe ip_nat_ftp 
  /sbin/modprobe ip_queue 
  /sbin/modprobe ip_conntrack 
  /sbin/modprobe ip_conntrack_ftp 
  /sbin/modprobe ip_tables 
  /sbin/modprobe iptable_filter 
  /sbin/modprobe iptable_nat 
  /sbin/modprobe iptable_mangle 
  /sbin/modprobe iptable_raw 
  /sbin/modprobe ipt_state 
  /sbin/modprobe ipt_limit 
  /sbin/modprobe ipt_multiport 
  /sbin/modprobe ipt_mac 
  /sbin/modprobe ipt_string 

start(){ 
  echo "Firewall iniciando ..............................[OK]" 
  #Limpa as regras 
  iptables -F 
  iptables -X 
  iptables -Z 
  iptables -F INPUT 
  iptables -F OUTPUT 
  iptables -F FORWARD 
  iptables -t nat -F 
  iptables -t nat -X 
  iptables -t nat -Z 
  iptables -t mangle -F 
  iptables -t mangle -X 
  iptables -t mangle -Z 
  iptables -t raw -F 
  iptables -t raw -X 
  iptables -t raw -Z 
  #definindo as regras padrão 
  iptables -P INPUT DROP 
  iptables -P OUTPUT DROP 
  iptables -P FORWARD DROP 
  
  #Proxy(Squid) - Redirecionando tráfego porta 80 para porta 3128(Squid) 
  iptables -t nat -A PREROUTING -p tcp -m multiport -s $LAN --dport 80,443 -j REDIRECT --to-ports 3128 
  echo "Redirecionando tráfego porta 80 para porta 3128(Squid) " 

  #roteamento de pacotes 
  echo 1 > /proc/sys/net/ipv4/ip_forward 
  #compartilhamento de conexão <br />
  iptables -t nat -A POSTROUTING -s $LAN -o $ifaceExt -j MASQUERADE 
  iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT 
  echo "compartilhamento da rede ativo" 
  
  #Libera a interface de loopback(localhost) 
  iptables -A INPUT -i lo -j ACCEPT 
  
  #liberando acesso pela internet (eth0=ifaceExt) 
  iptables -A INPUT -s $LAN -p tcp --dport 53 -j ACCEPT 
  iptables -A INPUT -s $LAN -p udp --dport 53 -j ACCEPT 
  
  #liberando acessos de alguns serviços (eth1=ifaceInt) e serviço DNS/FTP/SSH/SAMBA apenas para rede interna 
  #chain INPUT
  iptables -A INPUT -p tcp --dport 3128 -i $ifaceInt -j ACCEPT 
  #chain FORWARD 
  iptables -A FORWARD -p tcp --dport 3128 -i $ifaceInt -o $ifaceExt -s $LAN -j ACCEPT 
  iptables -A FORWARD -p tcp --dport 53 -i $ifaceInt -o $ifaceExt -s $LAN -j ACCEPT 
  iptables -A FORWARD -p udp --dport 53 -i $ifaceInt -o $ifaceExt -s $LAN -j ACCEPT 
  iptables -A FORWARD -p tcp --dport 21 -i $ifaceInt -o $ifaceExt -s $LAN -j ACCEPT 
  iptables -A FORWARD -p tcp --dport 20 -i $ifaceInt -o $ifaceExt -s $LAN -j ACCEPT 
  iptables -A FORWARD -p tcp --dport 110 -i $ifaceInt -o $ifaceExt -s $LAN -j ACCEPT 
  iptables -A FORWARD -p tcp --dport 25 -i $ifaceInt -o $ifaceExt -s $LAN -j ACCEPT 
  iptables -A FORWARD -p tcp --dport 3389 -i $ifaceInt -o $ifaceExt -s $LAN -j ACCEPT 
  iptables -A FORWARD -p tcp --dport 137:139 -i $ifaceInt -s $LAN -j ACCEPT 
  iptables -A FORWARD -p tcp --dport 137:139 -i $ifaceInt -s $LAN -j ACCEPT 
  #chain OUTPUT 
  iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT 
  iptables -A OUTPUT -p tcp --dport 443 -j ACCEPT 
  iptables -A OUTPUT -p tcp --dport 53 -j ACCEPT 
  iptables -A OUTPUT -p udp --dport 53 -j ACCEPT 
  #técnica Port knocking para SSH 
  #Fases do Segredo (portas para liberar acesso ssh) 
  iptables -N INTO-FASE2 
  iptables -A INTO-FASE2 -m recent --name FASE1 --remove 
  iptables -A INTO-FASE2 -m recent --name FASE2 --set 
  iptables -A INTO-FASE2 -j LOG --log-prefix "INTO FASE2:" 

  iptables -N INTO-FASE3 
  iptables -A INTO-FASE3 -m recent --name FASE2 --remove 
  iptables -A INTO-FASE3 -m recent --name FASE3 --set 
  iptables -A INTO-FASE3 -j LOG --log-prefix "INTO FASE3:" 

  iptables -N INTO-FASE4 
  iptables -A INTO-FASE4 -m recent --name FASE3 --remove 
  iptables -A INTO-FASE4 -m recent --name FASE4 --set 
  iptables -A INTO-FASE4 -j LOG --log-prefix "INTO FASE4:" 

  iptables -A INPUT -p tcp -m recent --update --name FASE 

  #Determina o número de portas e o tempo que o ip ficará em cada uma das fases aguardando a próxima porta ser digitada 
  iptables -A INPUT -p tcp --dport 1002 -m recent --set --name FASE1 
  iptables -A INPUT -p tcp --dport 1004 -m recent --rcheck --seconds 15 --name FASE1 -j INTO-FASE2 
  iptables -A INPUT -p tcp --dport 1006 -m recent --rcheck --seconds 15 --name FASE2 -j INTO-FASE3 
  iptables -A INPUT -p tcp --dport 1008 -m recent --rcheck --seconds 15 --name FASE3 -j INTO-FASE4 

  #Aqui chegamos a FASE4, que é a última porta, onde será liberada a conexão com a porta 22(ssh). 
  #O tempo está setado para 3600 segundos(1 hora). <br />Depois disso será fechada novamente para o ip em questão, lembrando que se ele ainda tiver logado não fará diferença, será fechada mesmo assim. Então aumente o tempo conforme desejado. <br />
  iptables -A INPUT -p tcp -s 0/0 --dport 22 -m recent --rcheck --seconds 3600 --name FASE4 -j ACCEPT 
  #fecha se tiver inativo por 3 minutos 
  #iptables -A INPUT -p tcp -s 0/0 --dport 22 -m limit --limit 3/minute  --limit-burst 3 -j DROP 
  #Por último fechamos todos acessos a porta 22(ssh) 
  iptables -A INPUT -p tcp --dport 22 -j DROP 

  #algumas proteções 
  #Filtrando pacotes ICMP 
  echo "Filtrando pacotes contra ICMP Broadcast" 
  echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts 
  echo "Protegendo contra Ping da Morte..." 
  iptables -A INPUT -p icmp --icmp-type echo-request -m limit --limit 1/s -j ACCEPT
  echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all
  iptables -A INPUT -p icmp -j DROP 
  #Bloqueando Tracerouters 
  #Bloqueia pacotes tcp malformados 
  iptables -A FORWARD -p tcp ! --syn -m state --state NEW -j DROP 
  iptables -A INPUT -m state --state INVALID -j DROP 
  iptables -A OUTPUT -m state --state INVALID -j DROP 
  iptables -A FORWARD -m state --state INVALID -j DROP 
  #Filtro das conexões estabelicidas 
  echo  "Permitindo e filtrando conexões estabelecidas" 
  iptables  -t filter -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
  iptables -t filter -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
  #iptables -t filter -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
  #Proteção contra Synflood
  iptables -N syn-flood
  iptables -A INPUT -i $ifaceInt -p tcp --syn -j syn-flood
  iptables -A syn-flood -m limit --limit 1/s --limit-burst 4 -j RETURN
  iptables -A syn-flood -j DROP
  iptables -A FORWARD -p tcp --syn -m limit --limit 1/s -j ACCEPT
  #Proteção contra falhas de segurança dos serviços do X Window
  iptables -A INPUT -p tcp -s 0.0.0.0/0 -d 0.0.0.0/0 --dport 6000:6063 -j DROP
  #Proteção contra port-scanners ocultos
  iptables -A FORWARD -p tcp --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 1/s -j ACCEPT
  iptables -N SCANNER
  iptables -A SCANNER -j DROP
  iptables -A INPUT -p tcp --tcp-flags ALL FIN,URG,PSH -i $ifaceExt -j DROP
  iptables -A INPUT -p tcp --tcp-flags ALL NONE -i $ifaceExt -j DROP
  iptables -A INPUT -p tcp --tcp-flags ALL ALL -i $ifaceExt -j DROP
  iptables -A INPUT -p tcp --tcp-flags ALL FIN,SYN -i $ifaceExt -j DROP
  iptables -A INPUT -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -i $ifaceExt -j DROP
  iptables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -i $ifaceExt -j DROP
  iptables -A INPUT -p tcp --tcp-flags SYN,FIN SYN,FIN -i $ifaceExt -j DROP
  iptables -A FORWARD --protocol tcp --tcp-flags ALL SYN,ACK -j DROP
  #-m recent --update --hitcount=4 quer dizer se tentar passar pelo ip utilizando o nmap 4x será jogado na lista INVASOR
  iptables -A INPUT -m recent --update --hitcount 4 --name INVASOR --seconds 3600 -j DROP
  #indica bloqueio por 3600 segundos, ou seja, 1 hora o host que tentou realizar um scan na rede
  iptables -A INPUT -m recent --set --name INVASOR
  #Proteção contra IP Spoofing
  iptables -A INPUT -s $LAN -i $ifaceExt -j DROP
  for i in /proc/sys/net/ipv4/conf/*/rp_filter; do
   echo 1 > $i
  done
  #Protecao contra ataques DoS
  iptables -A INPUT -m state --state INVALID -j DROP
  iptables -A OUTPUT -p tcp ! --tcp-flags SYN,RST,ACK SYN -m state --state NEW -j DROP
  #Verifica pacotes fragmentados e os descarta<br />
  iptables -N VALID_CHECK
  iptables -A VALID_CHECK -p tcp --tcp-flags ALL FIN,URG,PSH -j DROP
  iptables -A VALID_CHECK -p tcp --tcp-flags ALL SYN,ACK,FIN,URG -j DROP
  iptables -A VALID_CHECK -p tcp --tcp-flags ALL ALL -j DROP
  iptables -A VALID_CHECK -p tcp --tcp-flags ALL FIN -j DROP
  iptables -A VALID_CHECK -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
  iptables -A VALID_CHECK -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
  iptables -A VALID_CHECK -p tcp --tcp-flags ALL NONE -j DROP
  #Bloqueia tudo que não foi especificado acima
  iptables -A INPUT -p tcp --syn -j DROP
  echo "Firewall ativo ---------------------------------[OK]"
 }
stop()
  #Limpa as regras
  iptables -F
  iptables -X
  iptables -Z
  iptables -F INPUT
  iptables -F OUTPUT
  iptables -F FORWARD
  iptables -t nat -F
  iptables -t nat -X
  iptables -t nat -Z
  iptables -t mangle -F
  iptables -t mangle -X
  iptables -t mangle -Z
  iptables -t raw -F
  iptables -t raw -X
  iptables -t raw -Z

  #reseta as politicas padrões, aceita tudo
  iptables -P INPUT ACCEPT
  iptables -P OUTPUT ACCEPT
  iptables -P FORWARD ACCEPT
  #compartilhamento de conexão
  echo 1 > /proc/sys/net/ipv4/ip_forward
  iptables -t nat -F POSTROUTING
  iptables -t nat -A POSTROUTING -o $ifaceExt -j MASQUERADE

  echo "Firewall desativado! -----------------------------[OK]"
  }
case "$1" in
 "start") start ;;
 "stop") stop ;;
 "restart") stop; start ;;
*)
 echo "Use os paramentros: start|stop|restart"<br />
 esac